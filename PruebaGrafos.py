'''
Created on 27 nov. 2020

@author: Cesar
'''
class Vertice:
    
    def __init__(self, x):
        if(x==None):
            pass
        self.nombre = x
        self.numVertice = -1
        
    def getNombre(self):
        return self.nombre
    def setNombre(self, nombre):
       self.nombre = nombre
    def getNumVertice(self):
        return self.numVertice
    def setNumVertice(self, numVertice):
       self.numVertice = numVertice
       
    def nomVertice(self):
        return self.nombre
    def equals(self, n):
        return self.nombre==n.nombre
    def asigVert(self, n):
        self.setNumVertice(n)
    
    def __str__(self):
        return self.nombre+"("+str(self.numVertice)+")"

class GrafoMatriz:
    
    
    
    def correcion(self):
        e=1
        while e==1:
            try:
                r = int(input())
            except:
                print("Ups, solo numero enteros mayores a 0")
                e=1
            else:
                if r>0:
                    e=0
                else:
                    print("Ups, solo numeros entero mayores a 0")
                    e=1
        return r

    def __init__(self, mx):
        if(mx==None):
            self.maxVerts
        self.maxVerts = mx
        self.matAd = [[]]
        for i in range(mx):
            self.matAd.append([])
            for j in range(mx):
                self.matAd[i].append(None)
        self.verts = []
        for i in range(mx):
            self.verts.append(Vertice(None))
            j=0
            while(i<mx):
                self.matAd[i][j]=0;
                i+=1
        self.numVerts=0
        
    def numVertice(self, vs):
        v = Vertice(vs)
        encontrado = 0
        i=0
        while(i<self.numVerts and encontrado==0):
            
            if(self.verts[i].equals(v)):
                encontrado=1
            if(encontrado==0):
                i+=1
        if(i<self.numVerts):
            return i 
        else:
            return -1
    def nuevoVertice(self, nom):
        esta = (self.numVertice(nom)>=0)
        if(not esta):
            v =Vertice(nom)
            v.asigVert(self.numVerts)
            try:
                self.verts[self.numVerts]=v
                self.numVerts=self.numVerts+1
            except:
                print("limite de vertices excedido")
    def nuevoArco(self,a,b):
        va=self.numVertice(a)
        vb=self.numVertice(b)
        try:
            if(va<0 or vb<0):
                print("Vertice no existe")
        finally:
            pass
        self.matAd[va][vb]=1
    def adyacente(self,a,b):
        va=self.numVertice(a)
        vb=self.numVertice(b)
        try:
            if(va<0 or vb<0):
                print("Vertice no existe")
        finally:
            pass
        return (self.matAd[va][vb]==1)
    def recorrerAnchura(self,g,org):
        w=None
        v = g.numVertice(org)
        CLAVE= -1
        
        if(v<0):
            print("Vertice origen no existe")
        
        cola = ColaLista()
        m = []
        for i in range(g.numVerts):
            m.append(CLAVE)
        m[v]=0
        cola.insertar(v)
        while(not cola.colaVacia()):
            cw = cola.quitar()
            w = cw 
            print(f'Vertice {g.verts[w]} visitado')
            for u in range(g.numVerts):
                try:
                    if(g.matAd[w][u]==1 and m[u]==CLAVE):
                        m[u]=m[w]+1
                        cola.insertar(u)
                except:
                    print("No existe ese vertice")
        return m

class Nodo:
    
    def __init__(self,x,n,y):
        if(x==None):
            self.dato=int()
        else:
            self.dato=x
        if(n==None):
            self.siguiente=None
        else:
            self.siguiente=n
        if(y==None):
            self.elemento=None
        else:
            self.elemento=y
    
    def getDato(self):
        return self.dato
    def getEnlace(self):
        return self.siguiente
    def setEnlace(self,enlace):
        self.siguiente=enlace

class ColaLista:
    
    def __init__(self):
        self.frente=None
        self.fin=None
    
    def insertar(self,e):
        elemento = Nodo(None,None,e)
        if(self.colaVacia()):
            self.frente=elemento
        else:
            self.fin.setEnlace(elemento)
        self._fin=elemento
    def quitar(self):
        aux=None
        if(not self.colaVacia()):
            aux = self.frente.elemento
            self.frente=self.frente.siguiente
        else:
            print("la cola esta vacia")
        return aux
    def borrarCola(self):
        while( self._frente!=None):
            self._frente=self._frente.siguiente
    def frenteCola(self):
        if(self.colaVacia()):
            print("la cola esta vacia")
            return None
        else:
            return self._frente.elemento
    def colaVacia(self):
        return (self.frente==None)
    
class Arco:
    
    def __init__(self,d,p):
        self.destino = d
        if(p==None):
            self.peso=0.0
        else:
            self.peso=p
    
    def getDestino(self):
        return self.destino
    def equals(self,n):
        a=n
        return (self.destino==a.destino)
        
class GrafoAdcia:
    
    def __init__(self,mx):
        self.tablAdc = [mx]
        self.numVerts = 0
        self.maxVerts = mx
    
    def numVertice(self, vs):
        v = Vertice(vs)
        encontrado = False
        i=0
        while(i<self.numVerts and not encontrado):
            encontrado=(self.tablAdc[i]==v)
            if(not encontrado):
                i+=1
        if(i<self.numVerts):
            return i
        else:
            return -1
        
class NodoPila:
    def __init__(self,x):
        self.elemento=x 
        self.siguiente=None
        
v1=""
v2=""
opcion = 0

print("Digite el numero de vertices maximos")
x = int(input())

g = GrafoMatriz(x)
while(opcion!=5):
    print("============= Menu =============")
    print("Digite 1 para Añadir un vertice")
    print("Digite 2 para Añdir un arco")
    print("Digite 3 para Saber si son adyacentes")
    print("Digite 4 para Recorrer en anchura");
    print("Digite 5 para ***SALIR***");
    opcion = g.correcion()
    if(opcion==1):
        g.nuevoVertice(str(input("ingrese el nombre del vertice:")))
    elif(opcion==2):
        v1=input("Ingrese el nombre del primer vertice: ")
        v2=input("Ingrese el nombre del segundo vertice: ")
        try:
            g.nuevoArco(v1, v2)
            print("El arco a sido añadidoArco anadido")
        except:
            print("Error, deben existir ambos vertices")
    elif(opcion==3):
        v1=input("Ingrese el nombre del primer vertice: ")
        v2=input("Ingrese el nombre del segundo vertice: ")
        try:
            if(g.adyacente(v1, v2)):
                print(f"Existe adyacencia entre el vertice {v1} y {v2}")
            else:
                print("No existe adyacentes")
        except:
            print("Alguno de los dos vertices ingresados no existe")
    elif(opcion==4):
        v1=input("Ingrese el nombre del vertice a recorrer: ")
        try:
            g.recorrerAnchura(g, v1)
            print()
        except:
            print("El vertice no existe")
    elif(opcion == 5):
        print("Gracias por usar el programa")
    else:
        print("opcion no valida")

